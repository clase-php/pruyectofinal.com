const modal = document.querySelector('#modal');
const overlay = document.querySelector('#overlay');


function showModal() {
    modal.classList.toggle("form__modal--active");
    overlay.classList.toggle("overlay-modal--active");
}