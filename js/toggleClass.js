/**
 * Este script aplica estilos a un <label> cuando su <input> contiene texto
 * Si se agregan más campos, se almacenan en las variables
*/



/*
  Se van a buscar todos los elementos con las clases .input y
  .form__text, y se van a guardar en sus respectivas variables
*/
const input = document.querySelectorAll(".input");
const label = document.querySelectorAll(".form__text");

// Inputs radio
const start = document.getElementById("start");
const registro = document.getElementById("registro");

// Nombre de la pestaña
const startLabel = document.getElementById("start-label");
const registroLabel = document.getElementById("registro-label");

// Formulario de inicio o registro
const boxLogin = document.getElementById("box-login");
const boxRegistro = document.getElementById("box-registro");



// Detecta que pestaña esta activa al cargar la pagina
window.onload = function() {
  viewForm();
}



// La función recibe el "id" del <input> que activa onchange=""
function changeText(e) {
  // Se recorre el arreglo de todos los elementos <input>
  for(let i = 0; i < input.length; i++) {
    // Se accede al "id" del <input>
    const id = input[i].id

    /**
     * Se compara el "id" de cada <input> con el que recibe la función
     * Si el "id" coincide, se sabe que <input> llamó a la función
    */
    if(e === id) {
      // Se accede al valor que contiene el <input> y se almacena
      const value = input[i].value
      // Se accede al <label> que corresponde al <input> que llamó a la función
      const valueLabel = label[i]


      /**
       * Se varifica que el valor del <input> contenga texto
       * Si tiene texto se le agrega esa clase a su <label>
      */
      if(value != "") {
         valueLabel.classList.add('form__text--is-active');
      } else {
         valueLabel.classList.remove('form__text--is-active');
      }
    }
  }
}



// Detecta al hacer click en una pestaña
function viewForm() {
  if(!start.checked) {
      startLabel.classList.remove('form__sesion-label--active');
      registroLabel.classList.add('form__sesion-label--active');
      boxLogin.classList.add('form__log--visible');
      boxRegistro.classList.remove('form__log--visible');
  }
  else {
      startLabel.classList.add('form__sesion-label--active');
      registroLabel.classList.remove('form__sesion-label--active');
      boxRegistro.classList.add('form__log--visible');
      boxLogin.classList.remove('form__log--visible');
  }
}

