<?php
    if($_POST) {
        // Variables para login
        $user = $_POST['user'];
        $pwd = $_POST['pwd'];
        // Variables para registro
        $name = $_POST['name'];
        $userReg = $_POST['userReg'];
        $mail = $_POST['mail'];
        $pwdReg = $_POST['pwdReg'];
        // Variables para tipos de usuarios y formulario
        $sesion = $_POST['sesion'];
        $typeUser = $_POST['typeUser'];
        // Id incrementar de la tabla, siempre se pasa en 0 y la base lo autoincrementa
        $idField = 0;
        // bandera
        $flag = 0;





        // Conexión con las base de datos
        $serverName = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'catalogomotos';
        $field = 'users';

        // Se verifica si se puede o no conectar con la base de datos
        $conn = mysqli_connect($serverName, $userName, $password, $database)
            or die ("No se ha podido conectar al servidor de la Base de datos");


        if($name === '' || $userReg === '' || $mail === '' || $pwdReg === '') {
            $message = 'Todos los campos son obligatorios';
        }
        else {
            $insert = "INSERT INTO $field
                (id_users, nombre, user, mail, pass, typeUser)
                VALUES ($idField, '$name', '$userReg', '$mail', '$pwdReg', '$typeUser')";

            // Valida que el query se ejecutó correctamente
            $resQuery = mysqli_query($conn, $insert);

            if($resQuery) {
                $message = 'Nuevo usuario regisatrado';
            }
            else {
                $message = 'No se ha podido registrar al usuario';
            }
        }



        // Se cierra la conexión con la base de datos
        $conn->close();
    }
?>






<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../css/main.css">
        <title>Contacto</title>
    </head>
    <body>
        <!-- Menu de navegacion -->
        <header class="header">
            <nav class="wrapper  header__nav">
                <a href="../index.html" class="img  header__logo">
                    <img src="../images/icons/moto-rod.svg" alt="Logo de la marca">
                </a>
                <ul class="header__menu">
                    <li class="header__list">
                        <a href="../about.html" class="header__item">
                            Quiénes somos
                        </a>
                    </li>
                    <li class="header__list">
                        <p class="header__item">
                            Modelos
                        </p>

                        <!-- Submenu -->
                        <ul class="header__submenu">
                            <li>
                                <a href="kawasaki.php" class="header__subitem">
                                    Kawasaki
                                </a>
                            </li>
                            <li>
                                <a href="yamaha.php" class="header__subitem">
                                    Yamaha
                                </a>
                            </li>
                            <li>
                                <a href="honda.php" class="header__subitem">
                                    Honda
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="header__list">
                        <a href="contact.php" class="header__item">
                            Contacto
                        </a>
                    </li>
                    <li>
                        <a href="login.php" class="btn  btn--login">
                            Login
                        </a>
                    </li>
                </ul>
            </nav>
        </header>



        <!-- About -->
        <main class="login  login--no-flex">
            <div class="login__background  login__background--contact">
            </div>

            <section class="wrapper  below-header">
                <div class="inside">
                    <h1 class="heading5  title">
                        Contacto
                    </h1>

                    <div class="about-content  about-content--no-top">
                        <p class="about-title">
                            Solicita una cotizacion y co gusto te atenderemos.
                            Sera un plazar atenderte.
                        </p>
                    </div>
                </div>


                <form
                    action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>"
                    method="POST"
                    class="form  form--contact">

                    <div id="box-registro" class="form__log">
                        <h2 class="heading4  form__title">
                            Cotiza una moto
                        </h2>

                        <div class="form__field">
                            <label for="name" class="form__label">
                                <img
                                    src="../images/icons/user.svg"
                                    alt="Usuario"
                                    class="form__icon">
                            </label>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                class="input  input--transparent"
                                maxlength="20"
                                onchange="changeText(id)">
                            <label for="name" class="form__text">
                                Nombre
                            </label>
                        </div>

                        <div class="form__field">
                            <label for="mail" class="form__label">
                                <img
                                    src="../images/icons/mail.svg"
                                    alt="Usuario"
                                    class="form__icon">
                            </label>
                            <input
                                type="email"
                                id="mail"
                                name="mail"
                                class="input  input--transparent"
                                maxlength="40"
                                onchange="changeText(id)">
                            <label for="mail" class="form__text">
                                Correo
                            </label>
                        </div>

                        <div class="form__field">
                            <label for="description" class="form__text  form__text--not">
                                Descripcion
                            </label>
                            <textarea
                                name="description"
                                id="description"
                                class="textarea  textarea--transparent">
                            </textarea>
                        </div>
                    </div>

                    <button aria-label="Enviar" class="form__send">
                        <input type="submit" class="btn  btn--large" value="Enviar">
                    </button>

                    <p class="form__alert-message">
                        <?php
                            if($_POST) {
                                echo $message;
                            }
                        ?>
                    </p>
                </form>
            </section>
        </main>

        <!-- Animacion del label -->
        <script src="../js/toggleClass.js"></script>
    </body>
</html>