<?php
    if($_POST) {
        // Variables para login
        $user = $_POST['user'];
        $pwd = $_POST['pwd'];
        // Variables para registro
        $name = $_POST['name'];
        $userReg = $_POST['userReg'];
        $mail = $_POST['mail'];
        $pwdReg = $_POST['pwdReg'];
        // Variables para tipos de usuarios y formulario
        $sesion = $_POST['sesion'];
        $typeUser = $_POST['typeUser'];
        // Id incrementar de la tabla, siempre se pasa en 0 y la base lo autoincrementa
        $idField = 0;
        // bandera
        $flag = 0;





        // Conexión con las base de datos
        $serverName = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'catalogomotos';
        $field = 'users';

        // Se verifica si se puede o no conectar con la base de datos
        $conn = mysqli_connect($serverName, $userName, $password, $database)
            or die ("No se ha podido conectar al servidor de la Base de datos");




        // Verifica que los datos son enviados desde el formulario de inicio
        // Si se manda la petición desde el form de inicio de sesión
        if($sesion === 'start') {
            if($user === '' || $pwd === '') {
                $message = 'Ambos campos son obligatorios';
            }
            else {
                $consulta = "SELECT * FROM " . $field;
                $ShowUser = "SELECT id_users FROM " . $field;

                // Resultado de consulta 1
                $result = mysqli_query($conn, $consulta)
                    or die ("No se ha podido conectar con la Base de datos");

                if($result) {
                    while($row = mysqli_fetch_array($result)) {
                        if($row['user'] == $user || $row['pass'] == $pwd) {
                            $flag++;

                            if($row['user'] == $user && $row['pass'] == $pwd) {
                                if($row['typeUser'] === 'Administrador') {
                                    header("Location:logAdmin/index.html");
                                    break;
                                }
                                else if($row['typeUser'] === 'Usuario') {
                                    header("Location:logUser/index.html");
                                    break;
                                }
                                else {
                                    header("Location:logProv/index.html");
                                    break;
                                }
                            }
                            else if($row['user'] == $user) {
                                $message = 'La contraseña es incorrecta';
                            }
                            else {
                                $message = 'El usuario es incorrecto';
                            }

                            break;
                        }
                    }

                    if($flag ==  0) {
                        $message = 'El usuario y la contraseña no exiten';
                    }
                }
            }
        }
        // Si se manda la petición desde el form de registro
        else {
            if($name === '' || $userReg === '' || $mail === '' || $pwdReg === '') {
                $message = 'Todos los campos son obligatorios';
            }
            else {
                $insert = "INSERT INTO $field
                    (id_users, nombre, user, mail, pass, typeUser)
                    VALUES ($idField, '$name', '$userReg', '$mail', '$pwdReg', '$typeUser')";

                // Valida que el query se ejecutó correctamente
                $resQuery = mysqli_query($conn, $insert);

                if($resQuery) {
                    $message = 'Nuevo usuario regisatrado';
                }
                else {
                    $message = 'No se ha podido registrar al usuario';
                }
            }
        }



        // Se cierra la conexión con la base de datos
        $conn->close();
    }
?>





<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../css/main.css">
        <title>Inicio de sesion</title>
    </head>
    <body>
        <!-- Menu de navegacion -->
        <header class="header">
            <nav class="wrapper  header__nav">
                <a href="../index.html" class="img  header__logo">
                    <img src="../images/icons/moto-rod.svg" alt="Logo de la marca">
                </a>
                <ul class="header__menu">
                    <li class="header__list">
                        <a href="../about.html" class="header__item">
                            Quiénes somos
                        </a>
                    </li>
                    <li class="header__list">
                        <p class="header__item">
                            Modelos
                        </p>

                        <!-- Submenu -->
                        <ul class="header__submenu">
                            <li>
                                <a href="kawasaki.php" class="header__subitem">
                                    Kawasaki
                                </a>
                            </li>
                            <li>
                                <a href="yamaha.php" class="header__subitem">
                                    Yamaha
                                </a>
                            </li>
                            <li>
                                <a href="honda.php" class="header__subitem">
                                    Honda
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="header__list">
                        <a href="contact.php" class="header__item">
                            Contacto
                        </a>
                    </li>
                    <li>
                        <a href="login.php" class="btn  btn--login">
                            Login
                        </a>
                    </li>
                </ul>
            </nav>
        </header>



        <main class="login">
            <div class="login__background">
            </div>
            <form
                action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>"
                method="POST"
                class="form">

                <!-- Inicio de sesión  -->
                <div id="box-login" class="form__log">
                    <h2 class="heading4  form__title">
                        Inicio de Sesión
                    </h2>

                    <div class="form__field">
                        <label for="user" class="form__label">
                            <img
                                src="../images/icons/user.svg"
                                alt="Usuario"
                                class="form__icon">
                        </label>
                        <input
                            type="text"
                            id="user"
                            name="user"
                            class="input  input--transparent"
                            maxlength="16"
                            onchange="changeText(id)">
                        <label for="user" class="form__text">
                            Usuario
                        </label>
                    </div>

                    <div class="form__field">
                        <label for="pwd" class="form__label">
                            <img
                                src="../images/icons/password.svg"
                                alt="Contraseña"
                                class="form__icon">
                        </label>
                        <input
                            type="password"
                            id="pwd"
                            name="pwd"
                            class="input  input--transparent"
                            maxlength="16"
                            onchange="changeText(id)">
                        <label for="pwd" class="form__text">
                            Contraseña
                        </label>
                    </div>
                </div>


                <!-- Registro  -->
                <div id="box-registro" class="form__log">
                    <h2 class="heading4  form__title">
                        Registro
                    </h2>

                    <div class="form__field">
                        <label for="name" class="form__label">
                            <img
                                src="../images/icons/user.svg"
                                alt="Usuario"
                                class="form__icon">
                        </label>
                        <input
                            type="text"
                            id="name"
                            name="name"
                            class="input  input--transparent"
                            maxlength="20"
                            onchange="changeText(id)">
                        <label for="name" class="form__text">
                            Nombre
                        </label>
                    </div>

                    <div class="form__field">
                        <label for="userReg" class="form__label">
                            <img
                                src="../images/icons/user.svg"
                                alt="Usuario"
                                class="form__icon">
                        </label>
                        <input
                            type="text"
                            id="userReg"
                            name="userReg"
                            class="input  input--transparent"
                            maxlength="16"
                            onchange="changeText(id)">
                        <label for="userReg" class="form__text">
                            Usuario
                        </label>
                    </div>

                    <div class="form__field">
                        <label for="mail" class="form__label">
                            <img
                                src="../images/icons/mail.svg"
                                alt="Usuario"
                                class="form__icon">
                        </label>
                        <input
                            type="email"
                            id="mail"
                            name="mail"
                            class="input  input--transparent"
                            maxlength="40"
                            onchange="changeText(id)">
                        <label for="mail" class="form__text">
                            Correo
                        </label>
                    </div>

                    <div class="form__field">
                        <label for="pwdReg" class="form__label">
                            <img
                                src="../images/icons/password.svg"
                                alt="Contraseña"
                                class="form__icon">
                        </label>
                        <input
                            type="password"
                            id="pwdReg"
                            name="pwdReg"
                            class="input  input--transparent"
                            maxlength="16"
                            onchange="changeText(id)">
                        <label for="pwdReg" class="form__text">
                            Contraseña
                        </label>
                    </div>

                    <p class="form__title-users">
                        Tipo de Usuario
                    </p>
                    <div class="form__field  form__field--users">
                        <div>
                            <input
                                type="radio"
                                id="client"
                                value="Usuario"
                                name="typeUser"
                                class="input--active  input--hidden"
                                checked>

                            <label for="client" class="form__option-users">
                                Usuario
                            </label>
                        </div>

                        <div>
                            <input
                                type="radio"
                                id="admin"
                                value="Administrador"
                                name="typeUser"
                                class="input--active  input--hidden">

                            <label for="admin" class="form__option-users">
                                Administrador
                            </label>
                        </div>

                        <div>
                            <input
                                type="radio"
                                id="prov"
                                value="Proveedor"
                                name="typeUser"
                                class="input--active  input--hidden">

                            <label for="prov" class="form__option-users">
                                Proveedor
                            </label>
                        </div>
                    </div>
                </div>

                <button aria-label="Enviar" class="form__send">
                    <input type="submit" class="btn  btn--large" value="Enviar">
                </button>

                <div class="form__options">
                    <div class="form__field  form__field--not  form__field--sesion">

                        <label
                            id="start-label"
                            for="start"
                            class="form__label  form__sesion-label"
                            onclick="viewForm()">
                            Inicio
                            <input
                                type="radio"
                                id="start"
                                value="start"
                                name="sesion"
                                class="input--hidden"
                                checked>
                        </label>
                    </div>
                    <div class="form__field  form__field--not  form__field--sesion">
                        <label
                            id="registro-label"
                            for="registro"
                            class="form__label  form__sesion-label  form__sesion-label--active"
                            onclick="viewForm()">
                            Registro
                            <input
                                type="radio"
                                id="registro"
                                value="registro"
                                name="sesion"
                                class="input--hidden">
                        </label>
                    </div>
                </div>

                <p class="form__alert-message">
                    <?php
                        if($_POST) {
                            echo $message;
                        }
                    ?>
                </p>
            </form>
        </main>

        <!-- Animacion del label -->
        <script src="../js/toggleClass.js"></script>
    </body>
</html>