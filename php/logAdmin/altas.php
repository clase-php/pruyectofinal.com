<?php
    if($_POST) {
        $name = $_POST['name'];
        $line = $_POST['line'];
        $agencia = $_POST['agencia'];
        $image = $_POST['image'];
        $description = $_POST['description'];
        $model = $_POST['model'];
        $price = $_POST['price'];
        $stock = $_POST['stock'];
        $especif = $_POST['especif'];
        $prov = $_POST['prov'];





        // Conexión con las base de datos
        $serverName = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'catalogomotos';
        $field = 'modelos';

        // Se verifica si se puede o no conectar con la base de datos
        $conn = mysqli_connect($serverName, $userName, $password, $database)
            or die ("No se ha podido conectar al servidor de la Base de datos");



        if($name === '' || $line === '' || $model === '' || $price === '' || $stock === '' || $image === '' || $description === '') {
            $message = 'Todos los campos son obligatorios';
        }
        else {
            $insert = "INSERT INTO $field
                (id_modelos, nombre, linea, agencia, imagen, descripcion, modelo, precio, stock, id_especific, id_prov)
                VALUES (0, '$name', '$line', '$agencia', '$image', '$description', $model, '$price', $stock, $especif, $prov)";

            // Valida que el query se ejecutó correctamente
            $resQuery = mysqli_query($conn, $insert);

            if($resQuery) {
                $message = 'Se ha agregado ' . $name . ' exitosamente';
            }
            else {
                $message = 'No se ha podido agregar el producto';
            }
        }



        // Se cierra la conexión con la base de datos
        $conn->close();
    }
?>





<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../../css/main.css">
        <title>Altas</title>
    </head>
    <body>
        <!-- Menu de navegacion -->
        <header class="header">
            <nav class="wrapper  header__nav">
                <a href="index.html" class="img  header__logo">
                    <img src="../../images/icons/moto-rod.svg" alt="Logo de la marca">
                </a>
                <ul class="header__menu">
                    <li class="header__list">
                        <a href="about.html" class="header__item">
                            Quiénes somos
                        </a>
                    </li>
                    <li class="header__list">
                        <p class="header__item">
                            Modelos
                        </p>

                        <!-- Submenu -->
                        <ul class="header__submenu">
                            <li>
                                <a href="kawasaki.php" class="header__subitem">
                                    Kawasaki
                                </a>
                            </li>
                            <li>
                                <a href="yamaha.php" class="header__subitem">
                                    Yamaha
                                </a>
                            </li>
                            <li>
                                <a href="honda.php" class="header__subitem">
                                    Honda
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="header__list">
                        <a href="altas.php" class="header__item">
                            Altas
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="bajas.php" class="header__item">
                            Bajas
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="edit.php" class="header__item">
                            Modificar
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="contact.php" class="header__item">
                            Contacto
                        </a>
                    </li>
                    <li>
                        <a href="../login.php" class="btn  btn--login">
                            ADMIN
                        </a>
                    </li>
                </ul>
            </nav>
        </header>



        <main class="wrapper">
            <form
                action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>"
                method="POST"
                class="form  form--large">


                <div class="form__log">
                    <h2 class="heading4  form__title">
                        Dar de alta un nuevo modelo
                    </h2>

                    <div class="form__container">
                        <div class="form__field">
                            <label for="name" class="form__text  form__text--not">
                                Nombre
                            </label>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                class="input"
                                maxlength="20">
                        </div>

                        <div class="form__field">
                            <label for="line" class="form__text  form__text--not">
                                Linea
                            </label>
                            <input
                                type="text"
                                id="line"
                                name="line"
                                class="input"
                                maxlength="16">
                        </div>
                    </div>

                    <div class="form__container">
                        <div class="form__field">
                            <label for="model" class="form__text  form__text--not">
                                Modelo
                            </label>
                            <input
                                type="text"
                                id="model"
                                name="model"
                                class="input"
                                maxlength="16">
                        </div>

                        <div class="form__field">
                            <label for="price" class="form__text  form__text--not">
                                Precio
                            </label>
                            <input
                                type="text"
                                id="price"
                                name="price"
                                class="input"
                                maxlength="16">
                        </div>
                    </div>

                    <div class="form__container">
                        <div class="form__field">
                            <label for="stock" class="form__text  form__text--not">
                                Stock
                            </label>
                            <input
                                type="text"
                                id="stock"
                                name="stock"
                                class="input"
                                maxlength="16">
                        </div>

                        <div class="form__field">
                            <label for="image" class="form__text  form__text--not">
                                Nombre de la imagen
                            </label>
                            <input
                                type="text"
                                id="image"
                                name="image"
                                class="input"
                                maxlength="16">
                        </div>
                    </div>

                    <div class="form__container">
                        <div class="form__field">
                            <label for="prov" class="form__text  form__text--not">
                                Proveedor
                            </label>
                            <select name="prov" id="prov" class="select">
                                <option value="1">Kawasaki S.A. de C.V.</option>
                                <option value="2">Yamaha S.A. de C.V.</option>
                                <option value="3">Honda S.A. de C.V.</option>
                            </select>
                        </div>

                        <div class="form__field">
                            <label for="especif" class="form__text  form__text--not">
                                Especificaciones
                            </label>
                            <select name="especif" id="especif" class="select">
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div>
                    </div>

                    <div class="form__field">
                        <label for="agencia" class="form__text  form__text--not">
                            Agencia
                        </label>
                        <select name="agencia" id="agencia" class="select">
                            <option value="Kawasaki">Kawasaki</option>
                            <option value="Yamaha">Yamaha</option>
                            <option value="Honda">Honda</option>
                        </select>
                    </div>

                    <div class="form__field">
                        <label for="description" class="form__text  form__text--not">
                            Descripcion
                        </label>
                        <textarea
                            name="description"
                            id="description"
                            class="input  textarea">
                        </textarea>
                    </div>
                </div>

                <button aria-label="Enviar" class="form__send">
                    <input type="submit" class="btn  btn--large" value="Agregar">
                </button>

                <p class="form__alert-message">
                    <?php
                        if($_POST) {
                            echo $message;
                        }
                    ?>
                </p>
            </form>
        </main>
    </body>
</html>