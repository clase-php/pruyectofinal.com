<?php
    // Conexión con las base de datos
    $serverName = 'localhost';
    $userName = 'root';
    $password = '';
    $database = 'catalogomotos';
    $field = 'modelos';


    // Se verifica si se puede o no conectar con la base de datos
    $conn = mysqli_connect($serverName, $userName, $password, $database)
        or die ("No se ha podido conectar al servidor de la Base de datos");


    $show = "SELECT * FROM " . $field;

    $result = mysqli_query($conn, $show)
        or die ("No se ha podido conectar con la Base de datos");


    if($_POST) {
        $nameModel = $_POST['nameModel'];
        $data = 'nombre';
        $flag = 0;

        $check = "SELECT " . $data . " FROM " . $field;
        $del = "DELETE FROM " . $field . " WHERE " . $data . "='" . $nameModel . "';";


        if ($nameModel === '') {
            $message = 'Debes llenar el campo';
        }
        else {
            $validate = mysqli_query($conn, $check)
            or die ("No se ha podido conectar con la Base de datos");

            while($row2 = mysqli_fetch_array($validate)) {
                if($row2[$data] == $nameModel) {
                    $flag++;
                    $borrar = mysqli_query($conn, $del);
                    $result = mysqli_query($conn, $show);
                    $message = 'Se ha eliminado la ' . $nameModel;

                    break;
                }
            }

            if($flag === 0) {
                $message = 'El modelo no existe';
            }
        }


        // Se cierra la conexión con la base de datos
        $conn->close();
    }
?>





<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../../css/main.css">
        <title>Bajas</title>
    </head>
    <body>
        <!-- Menu de navegacion -->
        <header class="header">
            <nav class="wrapper  header__nav">
                <a href="index.html" class="img  header__logo">
                    <img src="../../images/icons/moto-rod.svg" alt="Logo de la marca">
                </a>
                <ul class="header__menu">
                    <li class="header__list">
                        <a href="about.html" class="header__item">
                            Quiénes somos
                        </a>
                    </li>
                    <li class="header__list">
                        <p class="header__item">
                            Modelos
                        </p>

                        <!-- Submenu -->
                        <ul class="header__submenu">
                            <li>
                                <a href="kawasaki.php" class="header__subitem">
                                    Kawasaki
                                </a>
                            </li>
                            <li>
                                <a href="yamaha.php" class="header__subitem">
                                    Yamaha
                                </a>
                            </li>
                            <li>
                                <a href="honda.php" class="header__subitem">
                                    Honda
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="header__list">
                        <a href="altas.php" class="header__item">
                            Altas
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="bajas.php" class="header__item">
                            Bajas
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="edit.php" class="header__item">
                            Modificar
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="contact.php" class="header__item">
                            Contacto
                        </a>
                    </li>
                    <li>
                        <a href="../login.php" class="btn  btn--login">
                            ADMIN
                        </a>
                    </li>
                </ul>
            </nav>
        </header>



        <main id="overlay" class="wrapper overlay-modal">
            <form
                action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>"
                method="POST"
                class="form  form--large">


                <div class="form__log">
                    <h2 class="heading4  form__title">
                        Dar de baja un modelo existente
                    </h2>

                    <div class="form__container">
                        <div class="form__field">
                            <label for="nameModel" class="form__text  form__text--not">
                                Nombre del modelo a eliminar
                            </label>
                            <input
                                type="text"
                                id="nameModel"
                                name="nameModel"
                                class="input"
                                maxlength="16">
                        </div>
                    </div>
                </div>

                <div class="form__send">
                    <p class="btn  btn--large" onclick="showModal()">
                        Eliminar
                    </p>
                </div>

                <div id="modal" class="form__modal">
                    <div class="form__modal-wrapp">
                        <p class="form__del">
                            Estas seguro de quieres borrar?
                        </p>
                        <div class="form__flex">
                            <button aria-label="Enviar" class="form__send-modal">
                                <input type="submit" class="btn  btn--large" value="Eliminar">
                            </button>

                            <p
                                aria-label="Enviar"
                                class="btn  btn--red  btn--large  form__send-modal"
                                onclick="showModal()">
                                Cancelar
                            </p>
                        </div>
                    </div>
                </div>

                <p class="form__alert-message">
                    <?php
                        if($_POST) {
                            echo $message;
                        }
                    ?>
                </p>
            </form>


            <section class="inside  show-item">
                <h3 class="heading5  title">
                    Modelos existentes
                </h3>

                <div class="show-item__row">
                    <p class="show-item__item">Agencia</p>
                    <p class="show-item__item">Nombre</p>
                    <p class="show-item__item">Linea</p>
                    <p class="show-item__item">Imagen</p>
                    <p class="show-item__item">Precio</p>
                </div>
                <?php
                    while($row = mysqli_fetch_array($result)) {
                        echo
                            '
                            <div class="show-item__row">
                                <p class="show-item__item">' . $row['agencia'] . '</p>
                                <p class="show-item__item">' . $row['nombre'] . '</p>
                                <p class="show-item__item">' . $row['linea'] . '</p>
                                <p class="show-item__item">' . $row['imagen'] . '</p>
                                <p class="show-item__item">' . $row['precio'] . '</p>
                            </div>
                        ';
                    }
                ?>
            </section>
        </main>

        <!-- Mostrar y ocultar la modal -->
        <script src="../../js/modal.js"></script>
    </body>
</html>