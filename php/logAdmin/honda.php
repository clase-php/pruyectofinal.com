<?php
    // Conexión con las base de datos
    $serverName = 'localhost';
    $userName = 'root';
    $password = '';
    $database = 'catalogomotos';
    $field = 'modelos';
    $field2 = 'especificaciones';

    // Se verifica si se puede o no conectar con la base de datos
    $conn = mysqli_connect($serverName, $userName, $password, $database)
        or die ("No se ha podido conectar al servidor de la Base de datos");


    $showItems = "SELECT * FROM " . $field . " JOIN " . $field2 . " ON " . $field . ".id_especific=" . $field2 . ".id_especific;" ;

    $result = mysqli_query($conn, $showItems)
        or die ("No se ha podido conectar con la Base de datos");
?>




<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../../css/main.css">
        <title>Honda</title>
    </head>
    <body>
        <!-- Menu de navegacion -->
        <header class="header">
            <nav class="wrapper  header__nav">
                <a href="index.html" class="img  header__logo">
                    <img src="../../images/icons/moto-rod.svg" alt="Logo de la marca">
                </a>
                <ul class="header__menu">
                    <li class="header__list">
                        <a href="about.html" class="header__item">
                            Quiénes somos
                        </a>
                    </li>
                    <li class="header__list">
                        <p class="header__item">
                            Modelos
                        </p>

                        <!-- Submenu -->
                        <ul class="header__submenu">
                            <li>
                                <a href="kawasaki.php" class="header__subitem">
                                    Kawasaki
                                </a>
                            </li>
                            <li>
                                <a href="yamaha.php" class="header__subitem">
                                    Yamaha
                                </a>
                            </li>
                            <li>
                                <a href="honda.php" class="header__subitem">
                                    Honda
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="header__list">
                        <a href="altas.php" class="header__item">
                            Altas
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="bajas.php" class="header__item">
                            Bajas
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="edit.php" class="header__item">
                            Modificar
                        </a>
                    </li>
                    <li class="header__list">
                        <a href="contact.php" class="header__item">
                            Contacto
                        </a>
                    </li>
                    <li>
                        <a href="../login.php" class="btn  btn--login">
                            ADMIN
                        </a>
                    </li>
                </ul>
            </nav>
        </header>



        <main>
            <!-- Hero -->
            <section class="hero  hero--medium">
                <div class="hero__background  hero__background--honda">
                </div>

                <div class="wrapper  hero__body">
                    <h1 class="heading2  hero__title-motos">
                        La fidelidad en dos ruedas, y un motor para toda la vida.
                    </h1>
                </div>
            </section>


            <!-- Cards -->
            <section class="wrapper  inside  card">
                <h2 class="heading5  title">
                    Conoce nuestros modelos de Honda
                </h2>


                <!-- Muestra de los datos -->
                <?php
                    while($row = mysqli_fetch_array($result)) {
                        if($row['agencia'] == 'Honda') {
                            echo
                                '
                                <article class="card__article">
                                    <div class="img  card__image">
                                        <img src="../../images/'.$row['imagen'].'.png" alt="">

                                        <p class="card__img-describe">
                                            '.$row['agencia'].' &nbsp; '.$row['linea'].'
                                        </p>
                                    </div>


                                    <div class="card__body">
                                        <div class="card__content">
                                            <div>
                                                <h3 class="card__title">
                                                    '.$row['agencia'].' - '.$row['nombre'].'
                                                </h3>
                                                <p>'.$row['descripcion'].'</p>
                                            </div>
                                            <div class="card__model">
                                                <h4 class="heading6 subtitle">
                                                    Modelo '.$row['modelo'].'
                                                </h4>
                                                <P class="card__price">
                                                    $'.$row['precio'].' MX.
                                                </P>
                                            </div>
                                        </div>


                                        <ul class="card__list">
                                            <li role="Especificaciones">
                                                <h4 class="card__list-title">
                                                    Especificaciones
                                                </h4>
                                            </li>
                                            <li class="card__list-row">
                                                <p class="card__list-item">
                                                    Motor:
                                                </p>
                                                <p>'.$row['motor'].'</p>
                                            </li>
                                            <li class="card__list-row">
                                                <p class="card__list-item">
                                                    Cilindrada:
                                                </p>
                                                <p>
                                                    '.$row['cc'].'cc
                                                </p>
                                            </li>
                                            <li class="card__list-row">
                                                <p class="card__list-item">
                                                    Transmisión:
                                                </p>
                                                <p>'.$row['transmision'].'</p>
                                            </li>
                                            <li class="card__list-row">
                                                <p class="card__list-item">
                                                    Frenos delanteros:
                                                </p>
                                                <p>'.$row['frenosF'].'</p>
                                            </li>
                                            <li class="card__list-row">
                                                <p class="card__list-item">
                                                    Frenos traseros:
                                                </p>
                                                <p>'.$row['frenosB'].'</p>
                                            </li>
                                            <li class="card__list-row">
                                                <p class="card__list-item">
                                                    Neumático delantero:
                                                </p>
                                                <p>'.$row['neumaticoF'].'</p>
                                            </li>
                                            <li class="card__list-row">
                                                <p class="card__list-item">
                                                    Neumático trasero:
                                                </p>
                                                <p>'.$row['neumaticoB'].'</p>
                                            </li>
                                        </ul>
                                    </div>
                                </article>
                                '
                            ;
                        }
                    }
                ?>
            </section>
        </main>
    </body>
</html>